# Changelog

## Version 0.3.0 (2019-08-07)

- Fix incompatibility with JupyterLab >= 1.0.2
- Display repo name instead of repo path (requires JupyterLab >= 1.0.3)

## Version 0.2.1 (2019-07-16)

- Fix invalid npm package

## Version 0.2.0 (2019-07-09)

- Update for JupyterLab 1.0
- Simplify input logic for username selector
  (taken from jupyterlab-github)
- Fix lint warning
- Add warning about subgroups being not supported
- Add .gitlab-ci.yml

## Version 0.1.1 (2018-12-18)

- Fix browsing of projects with name that differs from path

## Version 0.1.0 (2018-12-12)

- Initial release
